<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Acceso a la página</title>
    </head>
    <body>
        <h1>Acceso correcto. Bienvenido <bean:write name="LoginForm" property="userName"></bean:write></h1>
    </body>
</html>
