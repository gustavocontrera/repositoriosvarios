<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fallo de Logeo</title>
    </head>
    <body>
        <div style="color:red">
            <h1>Nombre de usuario invalido <bean:write name="LoginForm" property="userName"></bean:write></h1>
        </div>
    </body>
</html>
