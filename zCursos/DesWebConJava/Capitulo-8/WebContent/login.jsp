<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Utilizando Validation</title>
    </head>
    <body>
        <div style="color:red">
            <html:errors />
        </div>
        <html:form action="/Login" >
            Nombre de Usuario : <html:text name="LoginForm" property="userName" /> <br>
            Contraseña : <html:password name="LoginForm" property="password" /> <br>
            <html:submit value="Login" />
        </html:form>
    </body>
</html>
