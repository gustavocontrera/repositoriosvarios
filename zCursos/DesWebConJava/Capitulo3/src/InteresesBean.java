
public class InteresesBean {
	private double inversion;

    private int meses;

    private double tasa;

    public void setInversion(double inversion) {
        this.inversion = inversion;
    }

    public void setMeses(int meses) {
        this.meses = meses;
    }

    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    public double getInversion() {
        return inversion;
    }

    public int getMeses() {
        return meses;
    }

    public double getTasa() {
        return tasa;
    }

    // El c�lculo de las ganancias
    public double getGanancias() {
        return inversion * (tasa / 100.0) * (meses / 12.0);
    }

}
