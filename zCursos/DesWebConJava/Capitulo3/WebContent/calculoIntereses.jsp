<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Calculador de intereses JSP</title>
</head>
<body>
<jsp:useBean id="intereses" class="InteresesBean">
	<jsp:setProperty name="intereses" property="*" />
</jsp:useBean>

Inversi&oacute;n : <jsp:getProperty name="intereses" property="inversion" /><br/>
Meses: <jsp:getProperty name="intereses" property="meses" /><br/>
Tasa Anual %: <jsp:getProperty name="intereses" property="tasa" /><br/>
<br/>
Ganancias: <jsp:getProperty name="intereses" property="ganancias" /><br/>

</body>
</html>