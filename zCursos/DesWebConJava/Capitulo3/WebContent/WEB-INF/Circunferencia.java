
public class Circunferencia {

	 /**
     * Radio
     */
    private double radio;

    /**
     * Posici�n del centro
     */
    private int x, y;

	/**
	 * Hueco o lleno
	 */
	private boolean hueco;

    /**
     * Un constructor sin par�metros
     */
    public Circunferencia() {
        super();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setRadio(double r) {
        this.radio = r;
    }

    public double getArea() {
        return Math.PI * radio * radio;
    }

	public boolean isHueco() {
		return hueco;
	}

    /**
     * Para establecer el �rea nueva y ser consistentes debemos modificar el radio
     * @param al �rea nueva que queremos para la circunferencia
     */
    public void setArea(double area) {
        radio = Math.sqrt(area / Math.PI);
    }

}
