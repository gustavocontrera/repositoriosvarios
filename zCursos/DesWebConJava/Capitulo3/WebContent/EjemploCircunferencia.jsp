<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<jsp:useBean id="circ1" class="Circunferencia" scope="session" />

<jsp:getProperty name="circ1" property="x" />

<!-- Establecemos en 5 el valor de la propiedad x del bean circ1 -->
<jsp:setProperty name="circ1" property="x" value="5" />

<!-- Cambiamos el valor de la propiedad y de circ1 al resultado de la expresión -->
<jsp:setProperty name="circ1" property="y" value="<%= Math.sqrt(49) %>" />

<!-- Seteamos el valor de la propiedad radio de circ1 al valor del parámetro del mismo nombre (radio) -->
<jsp:setProperty name="circ1" property="radio" />

<!-- Cambiamos el valor de la propiedad radio de circ1 al valor del parámetro 'ratio' -->
<jsp:setProperty name="circ1" property="radio" param="ratio"/>

<!-- Cambiamos todos los valores de las propiedades del bean circ1 a los valores de los parámetros del mismo nombre -->
<jsp:setProperty name="circ1" property="*" />


</body>
</html>