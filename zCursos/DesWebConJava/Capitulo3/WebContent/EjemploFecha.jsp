<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ejemplos cap�tulo 3</title>
</head>
<body>


Hola!<br/>
La fecha de hoy es <b><%= new java.util.Date() %></b>

<!-- Inicia secci�n tablas datos -->
<table>

</table>

<%-- Guardar resultado en la base de datos --%>

<%-- Declaramos el m�todo fibonacci --%>
<%! 
	private int fibonacci(int n) {
		if (n == 0) {
			return 0;
		}
		if (n == 1) {
			return 1;
		}
		else {
			return fibonacci(n-1)+fibonacci(n-2);
		}
	}
%>

Los primeros 10 n&uacute;meros de Fibonacci:<br/><br/>

<%
for (int i=0; i < 10; i++) {
%>
	Fibonacci(<%= i %>) =  <%= fibonacci(i) %><br/>
<%
}
%>

</body>
</html>