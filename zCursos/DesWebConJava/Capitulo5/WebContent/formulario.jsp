
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<%-- Este formulario se enviar� a la acci�n ingresoDatos --%>
<html:form action="DatosForm">

Nombre: <html:text property="nombre" />

<%-- El mensaje de error si algo falla respecto al nombre --%>
<b><html:errors property="nombre" /></b>
<br/>

Edad: <html:text property="edad" />

<%-- El mensaje de error si algo falla respecto a la edad --%>
<b><html:errors property="edad" /></b>
<br/>

Fumador: <html:checkbox property="fumador" />

<%-- El mensaje de error si algo falla respecto a la condici�n de fumador --%>
<b><html:errors property="fumador" /></b>
<br/>

<br/>
<html:submit />

</html:form>
