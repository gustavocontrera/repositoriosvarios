<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>



<%-- Simplemente mostramos los datos ingresados por el usuario, notar que los estamos tomando del objeto datosForm que fue inclu�do por Struts en el contexto --%>

<bean:write name="datosForm" property="nombre" /><br/>
<bean:write name="datosForm" property="edad" /><br/>
<bean:write name="datosForm" property="fumador" /><br/>

</body>
</html>