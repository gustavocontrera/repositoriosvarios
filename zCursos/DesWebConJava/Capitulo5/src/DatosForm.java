import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;

public class DatosForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Las propiedades
    private String nombre;

    private Integer edad;

    private boolean fumador;

    // Los m�todos accesores
    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public boolean isFumador() {
        return fumador;
    }

    public void setFumador(boolean fumador) {
        this.fumador = fumador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
 
    /* Sobreescribimos el m�todo reset para reestablecer el valor
     de la propiedad fumador */
    public void reset(ActionMapping map, HttpServletRequest req) {
        fumador = false;
    }

    /* Validamos el contenido de este formulario y agregamos 
    mensajes de error en caso de fallas */
    public ActionErrors validate(ActionMapping map, HttpServletRequest req) {
        ActionErrors ret = new ActionErrors();

        if (nombre == null || nombre.trim().equals("")) {
            ret.add("nombre", new ActionMessage("Falta ingresar nombre",
                            false));
        }
        if (edad == null) {
            ret.add("edad", new ActionMessage("Falta ingresar edad", false));
        }
        else {
            if (fumador && edad.intValue() < 18) {
                ret.add("fumador", new ActionMessage("Peque&ntilde;o fumador!",
                        false));
            }
        }

        return ret;
    }


}
