import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class IngresoDatosAction extends Action {

    
    public ActionForward execute(ActionMapping map, ActionForm form,
            HttpServletRequest req, HttpServletResponse res) throws Exception {
    
        /* No hacemos nada aqui, simplemente redirigimos a la p�gina de salida */
        return map.findForward("ok");
    }
}
