import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class VerFormularioAction extends Action {

    public ActionForward execute(ActionMapping map, ActionForm form,
            HttpServletRequest req, HttpServletResponse res) throws Exception {

        /* Guardamos la marca en la sesi�n para proteger la p�gina */
        saveToken(req);

		// Mostramos el JSP
        return map.findForward("ok");
    }
}
