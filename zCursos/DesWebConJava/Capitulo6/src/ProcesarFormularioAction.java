import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ProcesarFormularioAction extends Action {

    public ActionForward execute(ActionMapping map, ActionForm form,
            HttpServletRequest req, HttpServletResponse res) throws Exception {

        // �La marca es v�lida?
        if (isTokenValid(req)) {
            // Invalidamos la marca
            resetToken(req);

            // Procesar formulario
            
            
            return map.findForward("ok");
        }
        else {
            /* Marca inv�lida, devolvemos p�gina de error */
            return map.findForward("mal");
        }
    }
}
