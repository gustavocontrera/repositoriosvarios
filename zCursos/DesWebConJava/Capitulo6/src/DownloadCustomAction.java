import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DownloadAction;

public class DownloadCustomAction extends DownloadAction {

    protected StreamInfo getStreamInfo(ActionMapping map, ActionForm form,
            HttpServletRequest req, HttpServletResponse res) throws Exception {

// Una clase interna que implemente la interfaz
        return new StreamInfo() {
            public String getContentType() {
                return "text/plain";
            }

            public InputStream getInputStream() throws IOException {
                String texto = "texto a devolver";
                return new ByteArrayInputStream(texto.getBytes());
            }
        };
    }
}
