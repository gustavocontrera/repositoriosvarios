import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DownloadAction;

public class DownloadResourceAction extends DownloadAction {

    protected StreamInfo getStreamInfo(ActionMapping map, ActionForm form,
            HttpServletRequest req, HttpServletResponse res) throws Exception {

        return new ResourceStreamInfo("application/pdf",                getServlet().getServletContext(), "/pdf/mapreduce.pdf");
    }

    // Este archivo es grande, agrandamos el buffer
    protected int getBufferSize() {
        return 8192;
    }
}
