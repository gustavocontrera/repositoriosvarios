import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DownloadAction;

public class DownloadFileAction extends DownloadAction {

    protected StreamInfo getStreamInfo(ActionMapping map, ActionForm form,
            HttpServletRequest req, HttpServletResponse res) throws Exception {
        return new FileStreamInfo("image/gif", new File("/home/tulsi/logo.gif"));
    }
}
