

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Parametros
 */
@WebServlet("/Parametros")
public class Parametros extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Parametros() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Obtenemos un objeto Print Writer para enviar respuesta
		try {
					((ServletResponse) request).setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<HTML><HEAD><TITLE>Leyendo par�metros</TITLE></HEAD>");
		pw.println("<BODY BGCOLOR=\"#CCBBAA\">");
		pw.println("<H2>Leyendo par�metros desde un formulario html</H2><P>");
		pw.println("<UL>\n");
		pw.println("Te llamas " + request.getParameter("NOMBRE") + "<BR>");
		pw.println("y tienes "  + request.getParameter("EDAD") + " a�os<BR>");
		pw.println("</BODY></HTML>");
		pw.close();	
		} catch (Exception e) {
			PrintWriter pw = response.getWriter();
			pw.println("<HTML><HEAD><TITLE>ERROR</TITLE></HEAD>");
			pw.println("<BODY BGCOLOR=\"#CCBBAA\">");
			pw.println("<H2>Leyendo par�metros desde un formulario html</H2><P>" + e.getMessage());
			pw.println("</BODY></HTML>");
			pw.close();	
			
		}
		
	
	}

}
