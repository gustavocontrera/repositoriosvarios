

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PasarParametro2
 */
@WebServlet("/PasarParametro2")
public class PasarParametro2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PasarParametro2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out;
		String title="Leyendo todos los parámetros";
		response.setContentType ("text/html");
		
		out = response.getWriter();
		out.println("<HTML><HEAD><TITLE>");
		out.println(title);
		out.println("</TITLE></HEAD><BODY>");
		out.println("<H1 ALIGN=CENTER>"+ title +"</H1>");
		out.println("<TABLE BORDER=1 ALIGN=CENTER>");
		out.println("<TR><TH>Nombre de parámetro<TH>Valores de parámetro");
		
		Enumeration<?> nombresDeParam = request.getParameterNames();
		
		while (nombresDeParam.hasMoreElements()){
			 String nombreParam = (String) nombresDeParam.nextElement();
			 out.println("<TR><TD>" + nombreParam);
			 out.println("<TD>");
			 String[] valoresDeParam= request.getParameterValues(nombreParam);
			 	if (valoresDeParam.length == 1) {
			 		String valorParam = valoresDeParam[0];
					if (valorParam.length()== 0)
						out.print("<I>No existe valor</I>");
					else
						out.print(valorParam);
			 	} 
			 	else { 
			 		out.println("<UL>");
			 		for (int i=0; i<valoresDeParam.length; i++) {
			 			out.println("<LI>" + valoresDeParam[i]);
			 		}
			 			out.println("</UL>");
			 		}
		 	}
		 out.println("</TABLE>");
		 out.println("</BODY></HTML>");
		 out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 doGet(request,response);
	}

}
