

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AhorcadoServlet
 */
@WebServlet("/AhorcadoServlet")
public class AhorcadoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
	
	// Las palabras disponibles para jugar
    private static final String[] PALABRAS = { "GATO", "CAPILLA", "BABOR",
            "MURCIELAGO", "VENTANAL", "HAMACA" };


    /**
     * @see HttpServlet#HttpServlet()
     */
    public AhorcadoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();

        int maxIntentos = 5;

// La palabra sobre la que est� adivinando
        String palabra = (String) sesion.getAttribute("palabra");

// Las letras que acert� 
       String aciertos; 

// Las letras que no acert�
        String errados; 

/* Primera vez del usuario, no tiene palabra asignada */
        if (palabra == null) {
            Random rand = new Random();

/* Le agregamos una al azar de las disponibles */
            palabra = PALABRAS[rand.nextInt(PALABRAS.length)];
            aciertos = "";
            errados = "";

/* Guardamos los datos iniciales en �sesion� */
            sesion.setAttribute("palabra", palabra);
            sesion.setAttribute("aciertos", aciertos);
            sesion.setAttribute("errados", errados);
        }
        else {

/* Obtenemos los datos de este usuario de �sesion� */
            aciertos = (String) sesion.getAttribute("aciertos");
            errados = (String) sesion.getAttribute("errados");

/* Verificamos si la letra que arriesg� pertenece o no a la palabra */
            String letra = request.getParameter("letra");
            if (palabra.indexOf(letra) >= 0) {
                aciertos += letra;
            }
            else {
                errados += letra;
            }

// Guardamos los datos actualizados en �sesion�
            sesion.setAttribute("aciertos", aciertos);
            sesion.setAttribute("errados", errados);
        }

// Imprimimos el resultado
        PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>AHORCADO</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>");

/* Iteramos por las letras de la palabra. Si ya la acert�, la mostramos, si no, mostramos un "_" */
        for (int i = 0; i < palabra.length(); i++) {
            String letra = palabra.substring(i, i + 1);
            if (aciertos.indexOf(letra) >= 0) {
                out.println(" " + letra);
            }
            else {
                out.println(" _");
            }
        }
        out.println("</h1>");
        out.println("<br/>");

/* Nos fijamos si yerr� m�s de los intentos permitidos */
        if (maxIntentos > errados.length()) {

// Todav�a est� en juego
            out.println("Intentos: " + (maxIntentos - errados.length()));
            out.println("<br/>");
            
// Las chances restantes
            for (char c = 'A'; c <= 'Z'; c++) {
                if (aciertos.indexOf(Character.toString(c)) == -1
                        && errados.indexOf(Character.toString(c)) == -1) {
                    
/* Mostramos letra como opci�n si no fue arriesgada a�n */
                    out.println("<a href=\"AhorcadoServlet?letra=" + c + "\">" + c
                            + "</a>");
                }
            }
        }
        else { // juego terminado
            
/* Invalidamos �sesion�, limpiando todo su contenido */
            sesion.invalidate();
            out.println("<h2>Juego terminado!</h2>");
            out.println("<br/>");
            
/* Le damos la oportunidad de que juegue de nuevo */
            out.println("<a href=\"ahorcado\">Jugar de nuevo</a>");
        }
        out.println("</body>");
        out.println("</html>");

	}

}
