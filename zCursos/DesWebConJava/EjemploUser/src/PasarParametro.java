

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PasarParametro
 */
@WebServlet("/PasarParametro")
public class PasarParametro extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PasarParametro() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out;
		String title="Leyendo 3 parámetros";
		response.setContentType 
		("text/html");
		out = response.getWriter();
		out.println("<HTML><HEAD><TITLE>");
		out.println(title);
		out.println("</TITLE></HEAD><BODY>");
		out.println("<H1 ALIGN=CENTER>"+ title 
		+"</H1>");
		out.println("<UL>");
		out.println("<LI>param1: "+ 
		request.getParameter("param1"));
		out.println("<LI>param2: "+ 
		request.getParameter("param2"));
		out.println("<LI>param3: "+ 
		request.getParameter("param3"));
		out.println("</UL>");
		out.println("</BODY></HTML>");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
}
