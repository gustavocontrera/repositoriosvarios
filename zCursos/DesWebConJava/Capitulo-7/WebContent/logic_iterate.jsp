<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <!-- Declaramos que vamos a usar la librer�a de tags de beans y que la usaremos con el prefijo bean -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<!DOCTYPE h2 PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<h2>Logic</h2>
<body>

<%-- Importamos clases --%>
<%@ page import="java.util.*, java.net.URL" %>



<%
	// Una colecci�n de direcciones web
	Collection urls = new Vector();
	urls.add(new URL("http://www.google.com"));
	urls.add(new URL("http://onweb.tectimes.com"));
	urls.add(new URL("http://java.sun.com"));
	urls.add(new URL("http://struts.apache.org"));
	urls.add(new URL("http://java.sun.com/products/jsp/"));
	pageContext.setAttribute("colUrls", urls, PageContext.PAGE_SCOPE);
%>

<%-- Iteramos y mostramos todas las direcciones web --%>
<h3>Toda la colecci&oacute;n:</h3>
<logic:iterate id="url" name="colUrls">
	<bean:write name="url"/><br/>
</logic:iterate>

<%-- Iteramos y mostramos la propiedad host de las primeros dos elementos de la colecci�n --%>
<h3>Los primeros dos hosts:</h3>
<logic:iterate id="url" name="colUrls" length="2">
	<bean:write name="url" property="host" /><br/>
</logic:iterate>

<%-- Iteramos y mostramos la propiedad protocol de las primeros dos elementos de la colecci�n y su posici�n --%>
<h3>Las direcciones 3 y 4:</h3>
<logic:iterate indexId="i" id="url" name="colUrls" offset="2" length="2">
	<bean:write name="url" />
	(posici&oacute;n <bean:write name="i" />)
	<br/>
</logic:iterate>


</body>
</html>