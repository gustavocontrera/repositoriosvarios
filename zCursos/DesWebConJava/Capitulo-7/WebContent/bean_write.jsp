<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <!-- Declaramos que vamos a usar la librer�a de tags de beans y que la usaremos con el prefijo bean -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<!DOCTYPE h2 PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<h2>Fechas</h2>
<body>
<!-- Declaramos un objeto Date que har� las veces de bean -->
<jsp:useBean id="fecha" class="java.util.Date" />

<!-- Imprimimos el objeto -->
Fecha: <bean:write name="fecha" /><br/>

<!-- Imprimimos propiedades del objeto (�este c�digo es caduco!) -->
Dia: <bean:write name="fecha" property="date" /><br/>
Mes: <bean:write name="fecha" property="month" /><br/>
A&ntilde;o: <bean:write name="fecha" property="year" /><br/>

<!-- Imprimimos la fecha pero d�ndole un formato -->
Fecha con formato: <bean:write name="fecha" format="dd/MM/yyyy" /><br/>

<!-- Con formato, usando formatKey -->
Fecha con formatKey: <bean:write name="fecha" formatKey="fecha.formato" /><br/>

<h2>N&uacute;meros</h2><br/>

<%
	/* Declaramos un objeto de tipo Double y lo agregamos al scope de p�gina */
    Double pi = Math.PI;
    pageContext.setAttribute("pi", pi, PageContext.PAGE_SCOPE);
%>

<!-- Imprimimos el n�mero -->
PI: <bean:write name="pi" /><br/>

<!-- El n�mero, formateado -->
PI con 4 decimales: <bean:write name="pi" format="##.####" /><br/>

<!-- Uso del atributo ignore, el siguiente tag no arrojar� una excepci�n pese a que el bean no existe -->
<bean:write name="inexistente" property="prop" ignore="true" />


</body>
</html>