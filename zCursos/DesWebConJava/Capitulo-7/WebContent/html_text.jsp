<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <!-- Declaramos que vamos a usar la librer�a de tags de beans y que la usaremos con el prefijo bean -->

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<!DOCTYPE h2 PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html:html xhtml="true">

<%-- Foco en el campo edad --%>
<html:form action="procesarFormulario" focus="edad">

<%-- Un campo con estilo que cambia al haber un error --%>
Nombre: 
<html:text property="nombre" 
	errorStyle="font-family:Verdana;font-size:12px;" 
	style="font-family:Verdana;font-size:12px;background-color:#FFFF00;color:#FF0000" />
<br/>

<%-- Un campo acotado en tama�o --%>
Edad: <html:text property="edad" size="3" maxlength="3"/>
<br/>

<%-- Campo deshabilitado --%>
Campo deshabilitado: <html:text property="deshabilitado" disabled="true" />
<br/>

<%-- Campo de solo lectura y con valor por defecto --%>
Campo de s&oacute;lo lectura: <html:text property="readonly" readonly="true" value="No soy modificable" />
<br/>

</html:form>

</html:html>
