<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%
	/* Declaramos un objeto de tipo String y lo agregamos al scope de p�gina */
    String clave ="saludo.inicial";
    pageContext.setAttribute("clave", clave, PageContext.PAGE_SCOPE);
%>

<!-- Llamado directo -->
Saludo: <bean:message key="saludo.inicial"/><br/>

<!-- Llamado directo con par�metro -->
Saludo con parametro: <bean:message key="saludo.parametrico" arg0="Signore Rigoberto" /><br/>

<!-- Llamado directo con par�metro -->
Saludo con din&aacute;mico: <bean:message key="saludo.parametrico" arg0="<%= Double.toString(Math.PI * 10) %>" /><br/>

<!-- Llamado indirecto -->
Saludo indirecto: <bean:message name="clave" /><br/>

</body>
</html>