<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html:html>

<%-- Le pasamos el foco al tercer componente del grupo de botones pais --%>
<html:form action="procesarCheckboxForm">

Condimentos: <br/>

<%-- Objetos checkbox que env�an un valor que definimos nosotros --%>
<html:checkbox property="aceite" value="si" />Aceite<br/>
<html:checkbox property="vinagre" value="si" />Vinagre<br/>
<html:checkbox property="sal" value="si" />Sal<br/>

Pa&iacute;ses visitados:<br/>

<%-- Iteramos sobre el conjunto de pa�ses para generar los otros checkbox --%>
<logic:iterate id="itPais" name="checkboxForm" property="paises">
	<html:multibox property="paisesVisitados">

<%-- El cuerpo del tag es el valor que enviar� --%>
		<bean:write name="itPais" property="codigo" />
	</html:multibox>
	
	<%-- Escribimos el nombre del pais para el usuario --%>
	<bean:write name="itPais" property="nombre" />
	<br/>
</logic:iterate>

<br/>
<html:submit value="Enviar" />

</html:form>

</html:html>
