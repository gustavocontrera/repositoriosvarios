window.onload = function() {
	// Hace petici�n al servidor para traer Datos la primera vez
	traerRSS();	
	$("btnActualizar").onclick = traerRSS;
}

function traerRSS() {
	// Apaga el bot�n
	$("btnActualizar").disabled = true;
	// Pone el fondo del body en gris
	$$("body")[0].style.background = "#EEE";
	$Ajax("tecnologia.xml", {
		cache: false,     // No queremos que cachee al actualizar
		cartelCargando: "divLoading",
		tipoRespuesta: $tipo.XML,
		onfinish: procesarRSS,
		onerror: function() {
			alert("Ha ocurrido un error al recibir el RSS")	
		}
	});		
}

// Guarda globalmente el RSS
var rss;

function procesarRSS(xml) {
	rss = xml;
	// Prende el bot�n
	$("btnActualizar").disabled = false;
	// Pone el fondo del body en blanco
	$$("body")[0].style.background = "white";
			
	// Proceso el RSS y voy completando los datos	
	$("divTitulo").innerHTML = leerTextoEnTag(rss.getElementsByTagName("title")[0]);

	// Limpia los items anteriores
	$("divItems").innerHTML = "";
	
	var items = rss.getElementsByTagName("item");
	
	for (var i=0; i<items.length; i++) {
		// Creo un nuevo elemento div de men� en divItems
		var div = document.createElement("div");
		div.id = "item_" + i;
		div.onclick =  function() { verItem(this) };
		// Obtengo el t�tulo del item actual
		div.innerHTML = leerTextoEnTag(items[i].getElementsByTagName("title")[0]);			
		$("divItems").appendChild(div);
	}
	// Selecciona la primera
	verItem($("item_0"));
}

function verItem(elemento) {
	// Le saca la clase "seleccionado" al anterior
	try {
		$$("div.selected")[0].removeClassName("selected");
	} catch(e) {}
	// Le asigna una clase a la seleccionada
	$(elemento).addClassName("selected");
	// Extrae el n�mero dentro del ID
	var id = elemento.id.substring(5);
	var item = rss.getElementsByTagName("item")[id];
	
	$("lblTituloItem").innerHTML = leerTextoEnTag(item.getElementsByTagName("title")[0]);
	$("lblTextoItem").innerHTML = leerTextoEnTag(item.getElementsByTagName("description")[0]);
	$("lblFechaItem").innerHTML = leerTextoEnTag(item.getElementsByTagName("pubDate")[0]);
	
	var link = "<a href='" + leerTextoEnTag(item.getElementsByTagName("link")[0]) + "' target='_blank'>Ver Articulo</a>";
	$("lblLinkItem").innerHTML = link;
		
	new Effect.Highlight("divDetalle", {duration: 0.3});
}



function leerTextoEnTag(elemento) {
	if (elemento!=undefined)
		return elemento.firstChild.nodeValue;	
	else
	 	return "";
}