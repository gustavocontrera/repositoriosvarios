window.onload = function() {
	$("divVentana").hide();
	$("lnkMostrar").href="javascript:abrirVentana()";
	$("lnkCerrar").href="javascript:cerrarVentana()";
}

/** Abre una ventana (div) con un efecto de aparici�n
 * 
 */
function abrirVentana() {
	new Effect.Appear($("divVentana"));
}

/** Cierra la ventana (div) con un efecto
 * 
 */
function cerrarVentana() {
	new Effect.Fold($("divVentana"), {
		afterFinish: function() {
			alert("Se termin� de cerrar la ventana");
		}	
	});
}


