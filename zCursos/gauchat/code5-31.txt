<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Formularios</title>
  <style>
    :required{
      border: 2px solid #990000;
    }
    :optional{
      border: 2px solid #009999;
    }
  </style>
</head>
<body>
  <section>
    <form name="miformulario" method="get" action="procesar.php">
      <input type="text" name="minombre">
      <input type="text" name="miapellido" required>
      <input type="submit" value="Enviar">
    </form>
  </section>
</body>
</html>