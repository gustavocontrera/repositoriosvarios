function reproducir(cache){
  var nodoFuente = contexto.createBufferSource();
  nodoFuente.buffer = cache;
  var nodoCompresor = contexto.createDynamicsCompressor();
  nodoCompresor.threshold.value = -60;
  nodoCompresor.ratio.value = 10;

  nodoFuente.connect(nodoCompresor);
  nodoCompresor.connect(contexto.destination);
  nodoFuente.start(0);
}