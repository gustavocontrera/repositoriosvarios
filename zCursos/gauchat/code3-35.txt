body{
  text-align: center;

  -webkit-perspective: 500px;
  -moz-perspective: 500px;
  -webkit-perspective-origin: 50% 90%;
  -moz-perspective-origin: 50% 90%;
}
#principal{
  display: block;
  width: 500px;
  margin: 50px auto;
  padding: 15px;
  border: 1px solid #999999;
  background: #FFFFFF;
  -webkit-transform: rotate3d(0, 1, 0, 135deg);
  -moz-transform: rotate3d(0, 1, 0, 135deg);
}
#titulo{
  font: bold 36px verdana, sans-serif;
}