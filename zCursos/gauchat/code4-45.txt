<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Javascript</title>
  <style>
    .miclase{
      background: #DDDD00;
    }
  </style>
  <script>
    var elem;
    function iniciar(){
      elem = document.getElementById('principal');
      elem.addEventListener('click', cambiarclase);
    }
    function cambiarclase(){
      elem.className = "miclase";
    }
    addEventListener('load', iniciar);
  </script>
</head>
<body>
  <section id="principal">Mi contenido</section>
</body>
</html>