mijuego.cargar = function(){
  var cargador = new THREE.ColladaLoader();
  cargador.load('police.dae', function(collada){ mijuego.texturas.coche = collada; });

  var img = document.createElement('img');
  img.setAttribute('src', 'wet_asphalt.jpg');
  img.addEventListener('load', function(e){ mijuego.texturas.piso = e.target; });

  var img = document.createElement('img');
  img.setAttribute('src', 'wall.jpg');
  img.addEventListener('load', function(e){ mijuego.texturas.paredes = e.target; });

  var controlbucle = function(){
    if(mijuego.texturas.coche && mijuego.texturas.piso && mijuego.texturas.paredes){
      mijuego.crear();
    }else{
      setTimeout(controlbucle, 200);
    }
  };
  controlbucle();
};