<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>API WebRTC</title>
  <style>
    #caller, #receiver{
      float: left;
      margin: 5px;
      padding: 5px;
      border: 1px solid #000000;
    }
    #botones{
      clear: both;
      width: 528px;
      text-align: center;
    }
    #cajadatos{
      width: 526px;
      height: 300px;
      margin: 5px;
      padding: 5px;
      border: 1px solid #000000;
    }
  </style>
  <script src="webrtc.js"></script>
</head>
<body>
  <section id="caller">
    <video id="callermedia" width="250" height="200"></video>
  </section>
  <section id="receiver">
    <video id="receivermedia" width="250" height="200"></video>
  </section>
  <nav id="botones">
    <input type="button" id="botonllamar" value="Llamar">
    <input type="text" id="entrada" size="60" required>
    <input type="button" id="botonenviar" value="Enviar">
  </nav>
  <section id="cajadatos"></section>
</body>
</html>