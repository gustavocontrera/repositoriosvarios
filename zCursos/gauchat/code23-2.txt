<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>WebSocket</title>
  <link rel="stylesheet" href="websocket.css">
  <script src="websocket.js"></script>
</head>
<body>
  <section id="cajaformulario">
    <form name="formulario">
      <label for="comando">Comando: </label><br>
      <input type="text" name="comando" id="comando"><br>
      <input type="button" id="boton" value="Enviar">
    </form>
  </section>
  <section id="cajadatos"></section>
</body>
</html>