var cajadatos;
function iniciar(){
  cajadatos = document.getElementById('cajadatos');
  var misarchivos = document.getElementById('misarchivos');

  misarchivos.addEventListener('change', procesar);
}
function procesar(e){
  var archivos = e.target.files;
  cajadatos.innerHTML = '';
  var miarchivo = archivos[0];
  if(!miarchivo.type.match(/image.*/i)){
    alert('Seleccione una imagen');
  }else{
    cajadatos.innerHTML += 'Nombre: ' + miarchivo.name + '<br>';
    cajadatos.innerHTML += 'Tamaño: ' + miarchivo.size + ' bytes<br>';

    var lector = new FileReader();
    lector.addEventListener('load', mostrar);
    lector.readAsDataURL(miarchivo);
  }
}
function mostrar(e){
  var resultado = e.target.result;
  cajadatos.innerHTML += '<img src="' + resultado + '">';
}
addEventListener('load', iniciar);