var cajadatos, dd;
function iniciar(){
  cajadatos = document.getElementById('cajadatos');
  var boton = document.getElementById('grabar');
  boton.addEventListener('click', leerarchivo);
  webkitRequestFileSystem(TEMPORARY, 5*1024*1024, creardd, mostrarerror);
}
function creardd(sistema){
  dd = sistema.root;
}
function mostrarerror(e){
  alert('Error: ' + e.code);
}
function leerarchivo(){
  var nombre = document.getElementById('mientrada').value;
  dd.getFile(nombre, {create: false}, function(entrada) {
    entrada.file(leercontenido, mostrarerror);
  }, mostrarerror);
}
function leercontenido(archivo){
  cajadatos.innerHTML = 'Nombre: ' + archivo.name + '<br>';
  cajadatos.innerHTML += 'Tipo: ' + archivo.type + '<br>';
  cajadatos.innerHTML += 'Tamaño: ' + archivo.size + ' bytes<br>';

  var lector = new FileReader();
  lector.addEventListener('load', exito);
  lector.readAsText(archivo);
}
function exito(e){
  var resultado = e.target.result;
  cajadatos.innerHTML += 'Contenido: ' + resultado;
  document.getElementById('mientrada').value = '';
}
addEventListener('load', iniciar);