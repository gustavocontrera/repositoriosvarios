function reproducir(cache){
  var nodoFuente = contexto.createBufferSource();
  nodoFuente.buffer = cache;
  nodoFuente.loop = true;
  nodoFuente.connect(contexto.destination);

  nodoFuente.start(0);
  nodoFuente.stop(contexto.currentTime + 3);
}