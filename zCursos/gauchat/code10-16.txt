function iniciar(){
  var elem = document.getElementById('canvas');
  var canvas = elem.getContext('2d');
  canvas.font = "bold 24px verdana, sans-serif";
  canvas.textAlign = "start";
  canvas.textBaseline = "bottom";
  canvas.fillText("Mi Mensaje", 100, 124);

  var size = canvas.measureText("Mi Mensaje");
  canvas.strokeRect(100, 100, size.width, 24);
}
addEventListener("load", iniciar);