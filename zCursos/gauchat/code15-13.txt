var cajadatos, bd;
function iniciar(){
  cajadatos = document.getElementById('cajadatos');
  var boton = document.getElementById('buscar');
  boton.addEventListener('click', buscarobjetos);

  var solicitud = indexedDB.open('mibase');
  solicitud.addEventListener('error', mostrarerror);
  solicitud.addEventListener('success', comenzar);
  solicitud.addEventListener('upgradeneeded', crearbd);
}
function mostrarerror(e){
  alert('Error: ' + e.code + ' ' + e.message);
}
function comenzar(e){
  bd = e.target.result;
}
function crearbd(e){
  var base = e.target.result;
  var mialmacen = base.createObjectStore('peliculas', {keyPath: 'id'});
  mialmacen.createIndex('BuscarFecha', 'fecha', {unique: false});
}
function buscarobjetos(){
  cajadatos.innerHTML = '';
  var buscar = document.getElementById('fecha').value;

  var mitransaccion = bd.transaction(['peliculas']);
  var mialmacen = mitransaccion.objectStore('peliculas');
  var miindice = mialmacen.index('BuscarFecha');
  var mirango = IDBKeyRange.only(buscar);
  var nuevocursor = miindice.openCursor(mirango);
  nuevocursor.addEventListener('success', mostrarlista);
}
function mostrarlista(e){
  var cursor = e.target.result;
  if(cursor){
    cajadatos.innerHTML += '<div>' + cursor.value.id + ' - ' + cursor.value.nombre + ' - ' + cursor.value.fecha + '</div>';
    cursor.continue();
  }
}
addEventListener('load', iniciar);