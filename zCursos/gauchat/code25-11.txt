var contexto, nodoPanner, renderer, escena, camara, malla;
function iniciar(){
  var canvas = document.getElementById('canvas');
  var ancho = canvas.width;
  var alto = canvas.height;

  renderer = new THREE.WebGLRenderer({canvas: canvas});
  renderer.setClearColor(0xFFFFFF);
  escena = new THREE.Scene();
  camara = new THREE.PerspectiveCamera(45, ancho/alto, 0.1, 10000);
  camara.position.set(0, 0, 150);

  var geometria = new THREE.BoxGeometry(50, 50, 50);
  var material = new THREE.MeshPhongMaterial({color: 0xCCCCFF});
  malla = new THREE.Mesh(geometria, material);
  malla.rotation.y = 0.5;
  malla.rotation.x = 0.5;
  escena.add(malla);

  var luz = new THREE.SpotLight(0xFFFFFF, 1);
  luz.position.set(0, 100, 250);
  escena.add(luz);
  contexto = new webkitAudioContext();
  contexto.listener.setPosition(0, 0, 150);

  var url = 'engine.wav';
  var solicitud = new XMLHttpRequest();
  solicitud.responseType = "arraybuffer";
  solicitud.addEventListener('load', function(){
    if(solicitud.status == 200){
      var cache = contexto.createBuffer(solicitud.response, false);
      reproducir(cache);
      canvas.addEventListener('mousewheel', mover, false);
      renderer.render(escena, camara);
    }
  });
  solicitud.open("GET", url, true);
  solicitud.send();
}
function reproducir(cache){
  var nodoFuente = contexto.createBufferSource();
  nodoFuente.buffer = cache;
  nodoFuente.loop = true;

  nodoPanner = contexto.createPanner();
  nodoPanner.refDistance = 100;
  nodoFuente.connect(nodoPanner);
  nodoPanner.connect(contexto.destination);
  nodoFuente.start(0);
}
function mover(e){
  malla.position.z += e.wheelDeltaY / 5;
  nodoPanner.setPosition(malla.position.x, malla.position.y, malla.position.z);
  renderer.render(escena, camara);
}
addEventListener('load', iniciar);