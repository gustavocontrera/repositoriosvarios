function reproducir(cache){
  var nodoFuente = contexto.createBufferSource();
  nodoFuente.buffer = cache;

  nodoRetardo = contexto.createDelay();
  nodoRetardo.delayTime.value = 1;

  nodoFuente.connect(nodoRetardo);
  nodoRetardo.connect(contexto.destination);
  nodoFuente.start(0);
}