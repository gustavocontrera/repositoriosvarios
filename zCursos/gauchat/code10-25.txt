var canvas, img;
function iniciar(){
  var elem = document.getElementById('canvas');
  canvas = elem.getContext('2d');

  img = document.createElement('img');
  img.setAttribute('src', 'snow.jpg');
  img.addEventListener("load", modimagen);
}
function modimagen(){
  canvas.drawImage(img, 0, 0);

  var info = canvas.getImageData(0, 0, 175, 262);
  var pos;
  for(var x = 0; x < 175; x++){
    for(var y = 0; y < 262; y++){
      pos = (info.width * 4 * y) + (x * 4);
      info.data[pos] = 255 - info.data[pos];
      info.data[pos+1] = 255 - info.data[pos+1];
      info.data[pos+2] = 255 - info.data[pos+2];
    }
  }
  canvas.putImageData(info, 0, 0);
}
addEventListener("load", iniciar);