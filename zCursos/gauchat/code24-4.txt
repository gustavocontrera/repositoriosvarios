function configurarcamara(stream){
  var video = document.getElementById("callermedia");
  video.setAttribute('src', URL.createObjectURL(stream));
  video.play();

  var servidores = {"iceServers": [{"url": "stun: stun.l.google.com: 19302"}]};
  usuario = new webkitRTCPeerConnection(servidores);
  usuario.addStream(stream);
  usuario.addEventListener('addstream', configurarremoto);
  usuario.addEventListener('icecandidate', configurarice);
}
function mostrarerror(){
  console.log('Error');
}