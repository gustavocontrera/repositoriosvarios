Javascript


> Mayor de
< Menor de
<= Menor de o igual a
>= Mayor de o igual a
=== Igual a
!== No igual a

-------------------------------

Para crear un cuadro de dialogo tipo Pop Up:

confirm("Cuidado con esto!")
Alert("Cuidado con esto!")


Para ingreso de datos mediante un PopUp:

prompt(“¿Como te llamas?”)

Otro ejemplo:

var nombre = prompt("Dime tu nombre");
alert("Hola," + nombre);


Para que devuelva un resultado true (length=longitud de palabra o frase):

"Estoy haciendo códigos como un genio".length > 10;



Toma lo que está entre paréntesis y lo muestra en la consola:

console.log(2 * 5)
console.log("Hola")



Sentencia If

if ( "andres".length >= 7 ) {
console.log("Tenés un nombre largo");
}          

Otro ejemplo:

if ( balance >= 0 ) {
alert("el balance es positivo");
if (balance > 10000) {
alert("El balance es muy grande");
}
} else {
alert("El balance es negativo");
}

Otro ejemplo:

var a = 123;
var b = 123;

// Comprobación de igualdad

if ( a === b ) {
alert("Si, son iguales");
} else {
alert("No, NO son iguales");
}


If continuación

if ( "gustavo".length >= 7 )
{
alert("¡Tu nombre es largo!");
}
else
{
// ¿Qué tenemos que  hacer si la condición es false ? Escribilo acá:
alert("Tu nombre es corto")
}



var velocidad = 80;

// Completa la condición dentro de los ()s en la linea 4
if (velocidad >= 80) {
     // Usa console.log() para mostrar "bajá la velocidad"
     console.log("Bajá la velocidad");
}
else {
     // Usa console.log() para mostrar "Manejo seguro"
    console.log("Manejo Seguro");

}

------------------------

Switch

 var combustible = "Super98";

switch (combustible) {
case "Diesel":
alert("1,02E");
break;
case "Super97":
alert("1,45E");
break;
case "Super95":
alert("1,67E");
break;
default:
alert("Lo que has escrito no lo tengo en el catalogo de combustibles")
}

Se podría cambiar el Alert por este:

alert("El combustible: " + combustible + " no lo tengo en el catalogo")

------------------------------------

Función

//Así es como se ve una función

var dividirPorTres = function (number) {
    var val = number / 3;
    console.log(val);
};

//En la línea 12, llamamos a la función por su nombre
//Ahora la llamamos desde 'dividirportres'
//Le decimos a la computadora cuál es el número de entrada (es decir, 6)
//Entonces la computadora usa el código dentro de la función
dividirPorTres(9);

--------------

//A continuación está la función de saludo
//Observa la línea 7
//Podemos unir strings usando el signo de más +
//Para saber más, consulta la sugerencia

var saludo = function (nombre) {
    console.log("Qué bueno verte," + " " + nombre);
};

//¡En la línea 11, llamá a la función de saludo!
saludo ("Gustavo");

-------------

/Función bien escrita:
var calcular = function (numero) {
    var val = numero * 10;
    console.log(val);
};

//Función mal escrita con errores de sintaxis:

//saludo var func{nombre}(console.log(nombre))}

var saludo = function (nombre) {
    console.log("Hola "+nombre);
    };
saludo("Gustavo");


-----------------
Funcion (Video2brain)

function miFuncion() {
var a = 5;
var b = 10;
var c = 20;
var d = a + b + c;
alert("El valor de d es igual a : " + d);
}

miFuncion();
miFuncion();
miFuncion();
miFuncion();
miFuncion();


Otro ejemplo:

function suma(a,b){
     var resultado = a + b;
     return resultado;
}

var sumando = suma(12,14);

alert(sumando);

Otro ejemplo:

var a = 10;
var b = 10;

if ( a < b ) {
alert("No son iguales");
}

if ( a == b ) 
{
alert("Si que son iguales");
}

-----------------

RETURN

//El parámetro es un número, y hacemos operaciones matemáticas con ese parámetro
var porDos = function(numero) {
    return numero * 2;
};

// llama a porDos acá
var nuevoNumero = porDos (10);
console.log(nuevoNumero);


-------

// Definí un cuarto acá.
var unCuarto = function (numero) {
    return numero / 4;
    };

if (unCuarto(24) % 3 == 0 ) {
  console.log("La sentencia es verdadera");
} else {
  console.log("La sentencia es falsa");
}

----------------------------------

Instrucciones

Escribí una función llamada perimetroCaja que retorne el perímetro de un rectángulo.

Debe tener dos parámetros.

Una fórmula para el perímetro es longitud + longitud + ancho + ancho

Llamá a la función y pasa cualquier valor que quieras para la longitud y el ancho.

Consejo
Acordate de que en las funciones los parámetros van entre paréntesis ( ). Y el bloque de código reusable va entre llaves { }.

// Escribí tu función empezando en la línea 3

var perimetroCaja = function (largo, ancho) {
    return (largo*2)+(ancho*2);
    };
    perimetroCaja (5,4);


------------------------------------------------

var controlDescanso = function (cantHoras) {
    if (cantHoras >= 8) {
        return "¡Estás durmiendo suficiente! ¡Demasiado quizás!";
    }
    else {
        return "¡Cerrá los ojos un poco más!";
    }
}

controlDescanso(10);


--------------------------

Ejercicio Piedra, papel o tijera!!!

var usuarioElige = prompt("piedra, papel o tijera?");
var computadoraElige = Math.random();
if (computadoraElige <0,34){
     computadoraElige = "piedra";
}else if(computadoraElige <=0.67){
     computadoraElige = "papel";
}else{
     computadoraElige = "tijera";
}

console.log ("Vos elegiste " + usuarioElige);
console.log ("La computadora eligió " + computadoraElige);

var comparar = function (eleccion1, eleccion2) {
    if (eleccion1 === eleccion2) {
        return "¡Es un empate!";
    }
        else if (eleccion1 === "piedra") {
            if (eleccion2 === "tijera") {
                return "gana piedra";
            }
            else if (eleccion2 === "papel") {
                return "gana papel";
            }
        }
       
        else if (eleccion1 === "papel") {
            if (eleccion2 === "piedra") {
                return "gana papel";
            }
            else if (eleccion2 === "tijera" ) {
                return "gana tijera";
            }
        }
       
        else if (eleccion1 === "tijera") {
            if (eleccion2 === "piedra") {
                return "gana piedra";
            }
            else if (eleccion2 === "papel" ) {
                return "gana tijera";
            }
        }
}

comparar (usuarioElige, computadoraElige);

-------------------------------------------------------------------------------------------------------------------------

FOR 
++ aumenta en uno
-- disminuye en uno
i += x (incrementa de x en x) (i += 3 incrementa de 3 en 3)
i -= x (disminuye de x en x)

// Ejemplo de un ciclo for

for( var counter = 1; counter < 11; counter ++) {
     console.log(counter);
}

----

// Editá este bucle for  (i += 5 es lo mismo que i = i + 5) 

for (var i = 5; i < 51; i += 5) {
     console.log(i);
}

-------

for( var i = 10 ; i >= 0 ; i -- ) {
     console.log(i);
}

----------

//Escribí tu propio ciclo for
for (var i = 100; i > 0; i -= 5) {
     console.log(i);
}

----------

Arreglos (Arrays) (Va entre corchetes) (Sintaxis: var nombreArreglo = [datos, datos, datos];

var nombres = ["Mao", "Ghandi", "Mandela"];
var tamaños = [4, 6, 3, 2, 1, 9];
var mixto = [34, "dulces", "azul", 11];

Ej:

var chatarra = ["arbol", "perro", 82, 78];

console.log (chatarra)

-------------

//Practicá con las matrices

var datosChatarra = ["Eddie Murphy", 49, "maníes", 31];

console.log(datosChatarra[3])

---------------

//Vamos a mostrar en pantalla cada elemento de un arreglo usando un ciclo for

var ciudades = ["Misiones", "Salta", "Mendoza", "Buenos Aires", "perro", "laucha"];

for (var i = 0; i < ciudades.length; i++) {
    console.log("Me gustaría visitar" + " " + ciudades[i]);
}

Otro ejemplo:

var coleccion = [500,500,500,500,500,500,500]

var total= 0;

for ( var i = 0 ; i < coleccion.length ; i++ ) {
// Añadir el valor actual al total
total = total + coleccion[i];
}

// Una vez salimos del bucle
alert("el valor total es: "+total);


-------------------

CICLO WHILE

var caraMoneda = Math.floor(Math.random() * 2);

while(caraMoneda){
     console.log("¡Cara! Lanza de nuevo...");
     var caraMoneda = Math.floor(Math.random() * 2);
}
console.log("¡Seca! Lanzamiento concluido."); 

-------

var understand = true;

while(understand){
     console.log("¡Estoy aprendiendo sobre los ciclos 'while'!");
     understand = false;
}

Otro ejemplo:

var cantidad = 0;

// Crear el índice

var i = 1;

// Crear la condición

while ( i <= 10 ) {

cantidad = cantidad + 100
// Incrementar el indice
i++;

}

alert("el valor final es "+cantidad)
// El resultado da 1000


-----------

Lo anterior tambien se puede poner asi pero no conviene:

var bool = true;

while(bool === true){
    console.log("¡Menos es más!");
    bool = false;
}


Mejor asi:

var bool = true;

while(bool){
    console.log("¡Menos es más!");
    bool = false;
}

------------------

//¡Recuerda hacer que tu condición sea igual a 'true' fuera del ciclo!
var count = 0

var ciclo = function(argumento){
     while(argumento < 3){
          return "Estoy en un bucle"
          argumento++
     }

}

ciclo(count);

---------------------------

DO While


condicionCiclo = false;

do {
     console.log("¡Voy a dejar de hacer ciclos porque mi condición es " + String(condicionCiclo) + "!");    
} while (condicionCiclo === true);

------------------------

addEventListener(Gauchat)


<!DOCTYPE html>
<html lang="es">
<head>
     <title>Este texto es el título del documento</title>
     <script>
          function mostraralerta(){
          alert('hizo clic!');
     }
          function hacerclic(){
          var elemento=document.getElementsByTagName('p')[0];
          elemento.addEventListener(‘click’, mostraralerta, false);
     }
          window.addEventListener(‘load’, hacerclic, false);
     </script>
</head>
<body>
     <div id=”principal">
          <p>Hacer Clic</p>
          <p>No puede hacer Clic</p>
     </div>
</body>
</html>
