-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-12-2013 a las 19:30:44
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ventas`
--
CREATE DATABASE IF NOT EXISTS `ventas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ventas`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `vta_id` int(11) NOT NULL AUTO_INCREMENT,
  `vta_usu` varchar(20) NOT NULL,
  PRIMARY KEY (`vta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`vta_id`, `vta_usu`) VALUES
(1, 'pepegil'),
(2, 'loloesteban'),
(3, 'pepe'),
(4, 'lolojuancha'),
(5, 'roberto calo'),
(6, 'roberto calores');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE IF NOT EXISTS `venta` (
  `vta_id` int(11) NOT NULL AUTO_INCREMENT,
  `vta_usuario` varchar(20) NOT NULL,
  `vta_texto` varchar(50) NOT NULL,
  `vta_fecha` date NOT NULL,
  `vta_id_usu` int(11) NOT NULL,
  PRIMARY KEY (`vta_id`),
  KEY `vta_id_usu` (`vta_id_usu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- RELACIONES PARA LA TABLA `venta`:
--   `vta_id_usu`
--       `usuarios` -> `vta_id`
--

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`vta_id`, `vta_usuario`, `vta_texto`, `vta_fecha`, `vta_id_usu`) VALUES
(1, 'pepe', 'televisor de 23"  de poco uso', '2013-11-16', 3),
(2, 'pepe', 'una computadora de unos años de uso con achaques', '2013-11-09', 3),
(3, 'roberto calores', 'televisor de 20"  de poco uso', '2013-11-06', 6),
(4, 'lolo esteban', 'una computadora de unos años de uso con achaques', '2013-11-09', 2),
(5, 'pepe', 'televisión de 21 "  de 10 años se uso', '2013-10-09', 3),
(6, 'lolo esteban', 'una rodio de uso manul', '2013-11-04', 2),
(7, 'pepe gil', 'televisión de 21 "  de 1 año se uso', '2013-09-11', 1),
(8, 'roberto Calorimetro', 'una caña de pescar para tiburón completa', '2013-11-04', 5),
(9, 'lolo juancha', 'horno de micro hondas de 300 w', '2013-11-12', 4),
(10, 'lolo juancha', 'baño sauna para tener en su casa ', '2013-11-03', 4),
(11, 'roberto Calorimetro', 'una bat de beisball', '2013-11-20', 5),
(12, 'pepe', 'una computadora con window 8', '2013-11-15', 3);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`vta_id_usu`) REFERENCES `usuarios` (`vta_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
