-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-12-2013 a las 19:32:13
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `chamuyo`
--
CREATE DATABASE IF NOT EXISTS `chamuyo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `chamuyo`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE IF NOT EXISTS `mensajes` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_origen` varchar(255) NOT NULL,
  `msg_destino` varchar(255) NOT NULL,
  `msg_texto` int(255) NOT NULL,
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- RELACIONES PARA LA TABLA `mensajes`:
--   `usu_id`
--       `usuarios` -> `usu_id`
--

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`msg_id`, `msg_origen`, `msg_destino`, `msg_texto`) VALUES
(1, 'marcelo', 'choque', 0),
(2, 'juanca', 'Marcelina', 0),
(3, 'jebus', 'Milli', 0),
(4, 'albertito', 'Roberta', 0),
(5, 'Luis', 'Rintintin', 0),
(6, 'Matu', 'Felix', 0),
(7, 'Angelina', 'Matias', 0),
(8, 'VAnesa', 'Vanina', 0),
(9, 'Pedrito', 'Jimena', 0),
(10, 'Horacio', 'Juana', 0),
(11, 'Yolanda', 'Gregorio', 0),
(12, 'luciano', 'choque', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_monbre` varchar(255) NOT NULL,
  PRIMARY KEY (`usu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usu_id`, `usu_monbre`) VALUES
(1, 'nestor'),
(2, 'jose');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
