-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-12-2013 a las 19:33:02
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `clase`
--
CREATE DATABASE IF NOT EXISTS `clase` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `clase`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
  `alumnoID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `fechanac` datetime DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `dni` varchar(9) DEFAULT NULL,
  `estudios` varchar(10) DEFAULT NULL,
  `completos` varchar(2) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`alumnoID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`alumnoID`, `nombre`, `apellido`, `telefono`, `fechanac`, `email`, `dni`, `estudios`, `completos`, `password`) VALUES
(0000000049, 'eduardo', 'fernandez', '1324545636', '0000-00-00 00:00:00', 'sirocco2001@hotmail.com', '11998952', 'terciaro', 'Si', '3733735cf0'),
(0000000036, 'fernado', 'neuman', '4444', '1970-02-02 00:00:00', 'fede@gmail', '82737372', 'primero', 'Si', 'b728250709'),
(0000000033, 'estela', 'novoa', '1234-6567', '1932-06-07 00:00:00', 'gfd@dsar.c', '23156198', 'secundario', 'No', 'da39a3ee5e'),
(0000000038, 'Fernando', 'Gonzales', '3457-8845', '1976-02-10 00:00:00', 'dse@debg.c', '11998952', 'secundario', 'Si', 'bc12e77f75'),
(0000000018, 'esteban', 'vilvar', '1234-4567', '0000-00-00 00:00:00', 'bgfbrewbr@', '12345678', 'uni', 'No', '4f1946d963'),
(0000000045, 'sie_', 'dane', '', '0000-00-00 00:00:00', 'ert#@ert.c', '656376574', 'universita', 'Si', '69393bda4c'),
(0000000020, 'esryrrsbv', 'gfwetbvw', 'bbtw-ewqe', '0000-00-00 00:00:00', ' h t te et', '67345123', 'uni', 'No', '220aab740b'),
(0000000024, 'Esteban', 'GIL', '8896-3456', '1986-03-20 00:00:00', 'vbdfsb@sdd', '43566357', 'secundario', 'Si', 'cbbc913356'),
(0000000025, 'JOSE', 'GIL', '8896-3456', '0000-00-00 00:00:00', 'vbdfsb@sdd', '43566357', 'secundario', 'Si', 'cbbc913356'),
(0000000026, 'JOSE', 'GIL', '8896-3456', '0000-00-00 00:00:00', 'vbdfsb@sdd', '43566357', 'secundario', 'Si', '1575ce8768'),
(0000000028, 'JOSE', 'GIL', '8896-3456', '0000-00-00 00:00:00', 'vbdfsb@sdd', '43566357', 'secundario', 'Si', '1575ce8768'),
(0000000050, 'rosa', 'perez', '2321454654', '0000-00-00 00:00:00', 'sirocco2001@hotmail.com', '11998952', 'primero', 'No', 'da39a3ee5e6b4b0d3255bfef95601890afd80709'),
(0000000051, 'rosa', 'perez', '2321454654', '0000-00-00 00:00:00', 'sirocco2001@hotmail.com', '11998952', 'primero', 'No', 'b12250c5409071986c3f11fa0096deb832c8cf73'),
(0000000052, 'eyer', 'wefvb', '1234545678', '0000-00-00 00:00:00', '', '', 'primero', 'No', 'da39a3ee5e6b4b0d3255bfef95601890afd80709'),
(0000000053, 'eduardo', '', '', '0000-00-00 00:00:00', '', '', 'primero', 'No', ''),
(0000000054, 'eduardo', 'perez', '1234567879', '0000-00-00 00:00:00', 'sirocco2001@hotmail.com', '11998952', 'universita', 'Si', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(32) NOT NULL,
  `apellido` varchar(32) NOT NULL,
  `usuario` varchar(10) NOT NULL,
  `pass` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `usuario`, `pass`) VALUES
(1, 'Juan', 'Perez', 'admin', 'adminphp'),
(2, 'Jose', 'Gomez', 'edwrtre', '6357865'),
(3, 'Pablo', 'Rodriguez', 'pablo', '12345'),
(4, 'alberto', 'gil', 'alberto', '12345');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
