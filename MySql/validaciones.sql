-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-12-2013 a las 19:29:50
-- Versión del servidor: 5.5.32
-- Versión de PHP: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `validaciones`
--
CREATE DATABASE IF NOT EXISTS `validaciones` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `validaciones`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `usuario` varchar(60) NOT NULL,
  `password` varchar(80) NOT NULL,
  `email` varchar(60) NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `comentarios` varchar(140) NOT NULL,
  `anio` varchar(2) NOT NULL,
  `aceptar` varchar(2) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario`, `password`, `email`, `edad`, `fecha`, `comentarios`, `anio`, `aceptar`, `id`) VALUES
('adsdsad6', '657aa10fca61f05aa6f512f60e9d1da411ca4c8d', 'introspeccion@hohoh.c', 22, '0000-00-00', 'Hola, &iquest;c&oacute;mo est&aacute;s?', '3A', 'Si', 1),
('adsdsad6ggkfa', '657aa10fca61f05aa6f512f60e9d1da411ca4c8d', 'introspeccion@hohoh.c', 22, '2013-03-24', 'dsad', '1A', 'Si', 9),
('adsdsad6ggd', '657aa10fca61f05aa6f512f60e9d1da411ca4c8d', 'introspeccion@hohoh.c', 22, '2013-03-24', 'dsad', '1A', 'Si', 10);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
