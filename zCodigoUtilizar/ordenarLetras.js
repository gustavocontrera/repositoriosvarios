
/* 
Ordenar las siguientes letras del alfabeto seg�n cantidad de apariciones,
 de mayor a menor (en caso de empate, considerar �rden alfab�tico). 
 La respuesta debe estar en min�scula y sin letras repetidas.

eoaKsVVjZnjgRTGWglmqZvbobxRVCNgzesEsAlHdCSKYCujCsoCIcxFbwaXiIQniLZjk
LpvsiiqrMNeQQrCueCPrRAGRLzXLspCZViOOlncymSSxNboycfKTIbbPMSomtLYSrsNc
VGzUjMllRcAUZbNmvxllWbOkZrIoLSsVrZBmNOvzpHkljqeAmmEyqIrjrsXExDF
 */
var arrayOriginal=new Array();
var arraySplit=new Array();
var arrayOrdenado=new Array();
var arrayLetra=new Array();
var arrayCantLetra=new Array();
var arrayTemp=new Array();
var arrayFinalN=new Array();
var arrayFinalL=new Array();
var repetida=0;
var i=0;
var j=0;
var msg='';

arrayOriginal='eoaKsVVjZnjgRTGWglmqZvbobxRVCNgzesEsAlHdCSKYCujCsoCIcxFbwaXiIQniLZjkLpvsiiqrMNeQQrCueCPrRAGRLzXLspCZViOOlncymSSxNboycfKTIbbPMSomtLYSrsNcVGzUjMllRcAUZbNmvxllWbOkZrIoLSsVrZBmNOvzpHkljqeAmmEyqIrjrsXExDF';

//msg= 'el array original es:'+ arrayOriginal + '\n';
//alert(msg);

// SPLIT - ARMA UN ARRAY DE LETRAS SEPARADOS POR COMAS -
arraySplit='eoaKsVVjZnjgRTGWglmqZvbobxRVCNgzesEsAlHdCSKYCujCsoCIcxFbwaXiIQniLZjkLpvsiiqrMNeQQrCueCPrRAGRLzXLspCZViOOlncymSSxNboycfKTIbbPMSomtLYSrsNcVGzUjMllRcAUZbNmvxllWbOkZrIoLSsVrZBmNOvzpHkljqeAmmEyqIrjrsXExDF'.split('');

//msg= 'el array spliteado es:'+ arraySplit + '\n';
//alert(msg);

// SORT - ORDENA LAS LETRAS DEL ARRAY POR ORDEN ALFABETICO
arrayOrdenado=arraySplit.sort();

//msg= 'el array ordenado es:'+ arrayOrdenado + '\n';
//alert(msg);

// ESTE CICLO ARMA 2 NUEVOS ARRAYS RELACIONADOS = 1 CON C/LETRA INDIVIDUAL Y
// EL OTRO ARRAY CONTIENE LA CANTIDAD DE VECES QUE ESTA REPETIDA C/LETRA
for (i=0;i<arrayOrdenado.length - 1;i++)
{    repetida=repetida+1;
    if(arrayOrdenado[i]!== arrayOrdenado[i+1])
    {   arrayLetra[j]=arrayOrdenado[i];
        arrayCantLetra[j]=repetida;
        repetida=0;
        j=j+1;
    }
       // ESTE IF ES PARA SOLUCIONAR LA COMPARACION DE LA ANTEULTIMA LETRA
       // CON LA ULTIMA LETRA DEL arrayOrdenado 
        if(arrayOrdenado[i]!== arrayOrdenado[i+1])
        {  arrayLetra[j]=arrayOrdenado[i+1];
           arrayCantLetra[j]=1;
        }
        else{
              arrayLetra[j]=arrayOrdenado[i];
              arrayCantLetra[j]=repetida+1;
            }
}
//msg= 'el array Letra es: '+ arrayLetra + '\n';
//alert(msg);

//msg= 'el array cantidad de letras es:'+ arrayCantLetra + '\n';
//alert(msg);

msg= 'longitud de los arrays:'+ arrayLetra.length + '\n'
        + arrayCantLetra.length + '\n';
alert(msg);

// ESTE CICLO ORDENA, DE MAYOR A MENOR LA CANTIDAD DE VECES QUE ESTA REPETIDA
// CADA LETRA
for(i=0;i<arrayCantLetra.length - 1;i++)
    {
        for(j=i+1;j<arrayCantLetra.length;j++)
        {
            if(arrayCantLetra[i]<arrayCantLetra[j])
            {
                arrayTemp=arrayCantLetra[i]; 
                arrayCantLetra[i]=arrayCantLetra[j]; 
                arrayCantLetra[j]=arrayTemp;
                arrayTemp=arrayLetra[i]; 
                arrayLetra[i]=arrayLetra[j]; 
                arrayLetra[j]=arrayTemp;
            }
        }
    }

msg= 'arrays letras y cantidad respectiva: '+ arrayLetra + '\n'
        + arrayCantLetra + '\n';
alert(msg);

//RESULTADO FINAL: LA/S LETRA/S CON MAYOR CANTIDAD DE REPETICIONES
i=-1;
do{ 
    arrayFinalN[i+1]=arrayCantLetra[i+1];
    arrayFinalL[i+1]=arrayLetra[i+1];
    i=i+1;    
   }while (arrayCantLetra[i]===arrayCantLetra[i+1]);
   
msg= 'La letra mas repetida es:  '+ arrayFinalL + '\n con '
        + arrayFinalN + ' cantidad de veces';

alert(msg);

