
var HOTEL = {};

HOTEL.inicializar = function (){
    
    HOTEL.traerTemplate();
    HOTEL.json();
    HOTEL.mostrarHotel();
};

HOTEL.traerTemplate = function () {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "hotelesV.html", false);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
                   HOTEL.hotelV = xhr.responseText;       
        }
    };  
    xhr.send(null);
    
};

HOTEL.json = function () {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "data/hoteles.json", false);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
                HOTEL.dataJson = xhr.responseText;
        }
        
    };  
    xhr.send(null);   
};

HOTEL.mostrarHotel = function(){
    var Hoteles_ = JSON.parse(HOTEL.dataJson);
    var webHotel = document.querySelector("#listado");
    webHotel.innerHTML = Mustache.render(HOTEL.hotelV, Hoteles_);
};

HOTEL.inicializar();